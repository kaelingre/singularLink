(* ::Package:: *)

(* Wolfram Language Package *)

(* Created by the Wolfram Workbench Feb 2, 2018 *)

BeginPackage["singularLink`"]
(* Exported symbols added here with SymbolName::usage *) 
Unprotect@@Names["singularLink`*"];

singularLink::libNotFound = 
"C++ library could not be loaded.
 Please install the package in the directory: 
 " <> $UserBaseDirectory <> "/Applications/"
 
 (*Load shared C++ library*)
If[FindLibrary["libSingularLink"]===$Failed,Message[singularLink::libNotFound];Abort[]; LibraryLoad["libSingularLink"];]


slimgb::usage = "Singular slimgb"
slimgb::author = "Gregor Kaelin"

reduce::usage = "Singular reduce"
reduce::author = "Gregor Kaelin"

singularIO::usage = "Call singular"
singularIO::author = "Alessandro Georgoudis, Gregor Kaelin"

setSingularPath::usage = "Set the path to libSingular.so"
setSingularPath::author = "Gregor Kaelin"

Begin["`Private`"]
(* Implementation of the package *)

Options[singularIO] = {MonomialOrder -> DegreeReverseLexicographic, Modulus -> 0};
singularIO[Ideal_, Vars_, SingularCommand_, OptionsPattern[]] := Module[
{ParameterVars, RelabelVarsList, IdealInStringForm, ModulusAndConstants, RingVars, SingularMonomialOrderings, Stream, SingularResultInMathematicaForm, TmpRes}, 
ParameterVars = Complement[Variables[Ideal], Vars];
RelabelVarsList = Join[relabelVars[Vars, "z"], relabelVars[ParameterVars, "c"]];
IdealInStringForm = ToString[InputForm[Ideal /. RelabelVarsList]];
   
(*  Replace (...)^a/b  --> (...)^a*1/b  *)
(* IdealInStringForm=
   StringReplace[IdealInStringForm,"^"~~a:NumberString~~"/"~~b:
 NumberString:>"^"~~ToString[a]~~"*1/"~~ToString[b]]; *)
   
 ModulusAndConstants = StringReplace[ToString[Prepend[ParameterVars /. RelabelVarsList, OptionValue[Modulus]]], {"{" -> "(", "}" -> ")"}];
   RingVars = 
    StringReplace[
     ToString[Vars /. RelabelVarsList], {"{" -> "(", "}" -> ")"}];
   SingularMonomialOrderings = {DegreeReverseLexicographic -> "dp", 
     DegreeLexicographic -> "Dp", Lexicographic -> "lp"};
   (*  Create input file for Singular  *)
   Stream = "";
   Stream = 
    StringJoin[Stream, 
     "ring R = " <> ModulusAndConstants <> ", " <> RingVars <> 
      ", " <> (OptionValue[MonomialOrder] /. 
        SingularMonomialOrderings) <> ";\n"];
   Stream = StringJoin[Stream, "option(redSB); option(redTail);\n"];
   (*WriteString[Stream,"degBound = 0;\n"];*)
   (* WriteString[
   Stream,"degBound = "<>ToString[OptionValue[DegBound]]<>";\n"]; *)
 
     Stream = 
    StringJoin[Stream, 
     "ideal I = " <> 
      StringReplace[IdealInStringForm, {"{" -> "", "}" -> ""}] <> 
      ";\n"];
   Stream = StringJoin[Stream, SingularCommand <> "(I);\n"];
   Stream = StringJoin[Stream, "return();"];
   TmpRes = StringRiffle[StringSplit[#,"="][[2]]&/@StringSplit[singularInterpret[Stream], "\n"],","];
   SingularResultInMathematicaForm[0] = 
    "{" <> StringReplace[TmpRes, 
      "gen(" ~~ Shortest[a__] ~~ ")" :> "gen[" ~~ a ~~ "]"] <> "}";
   SingularResultInMathematicaForm[
     1] = (ToExpression[SingularResultInMathematicaForm[0]] /. 
      Global`gen[i_] :> UnitVector[Length[Ideal], i]);
   Return[
    SingularResultInMathematicaForm[
      1] /. (Reverse[#] & /@ RelabelVarsList)];
   ];


Options[slimgb] = {MonomialOrder -> DegreeReverseLexicographic, Modulus -> 0};
slimgb[ideal_List, vars_List, OptionsPattern[]] := Module[
	{params, relabel, idealInStringForm, modulusAndConstants, ringVars, stream, tmpRes},
	(* prepare variables and relabeling *) 
	params = Complement[Variables[ideal], vars];
	relabel = Join[relabelVars[vars, "z"], relabelVars[params, "c"]];
	idealInStringForm = ToString[InputForm[ideal/.relabel]];
   
	(*  Replace (...)^a/b  --> (...)^a*1/b  *)
	idealInStringForm = StringReplace[idealInStringForm,"^"~~a:NumberString~~"/"~~b:NumberString:>"^"~~ToString[a]~~"*1/"~~ToString[b]];
   
 	modulusAndConstants = StringReplace[ToString[Append[Prepend[params/.relabel, OptionValue[Modulus]], I]], {"{" -> "(", "}" -> ")"}];
   	ringVars = StringReplace[ToString[vars/.relabel], {"{" -> "(", "}" -> ")"}];
   	(*  Create input string for Singular  *)
   	stream = "ring r = " <> modulusAndConstants <> "," <> ringVars <> "," <> prepMonomialOrder[OptionValue[MonomialOrder]] <> ";"
    	<> "ideal i = " <> StringReplace[idealInStringForm, {"{" -> "", "}" -> ""}] <> ";"
    	<> "slimgb(i);kill r;return();\n";
    tmpRes = StringRiffle[StringSplit[#,"="][[2]]&/@StringSplit[singularInterpret[stream], "\n"],","];
   	tmpRes = "{" <> tmpRes <> "}";
   	ToExpression[tmpRes]/.(Reverse/@relabel)
];


Options[reduce] = {MonomialOrder -> DegreeReverseLexicographic, Modulus -> 0};
reduce[poly_, ideal_List, vars_List, OptionsPattern[]] := Module[
	{params, relabel, polyInStringForm, idealInStringForm, modulusAndConstants, ringVars, stream, tmpRes},
	(* prepare variables and relabeling *) 
	params = Complement[Join[Variables[ideal], Variables[poly]], vars];
	relabel = Join[relabelVars[vars, "z"], relabelVars[params, "c"]];
	idealInStringForm = ToString[InputForm[ideal/.relabel]];
	polyInStringForm = ToString[InputForm[poly/.relabel]];
   
	(*  Replace (...)^a/b  --> (...)^a*1/b  *)
	idealInStringForm = StringReplace[idealInStringForm,"^"~~a:NumberString~~"/"~~b:NumberString:>"^"~~ToString[a]~~"*1/"~~ToString[b]];
	polyInStringForm = StringReplace[polyInStringForm,"^"~~a:NumberString~~"/"~~b:NumberString:>"^"~~ToString[a]~~"*1/"~~ToString[b]];
   
	modulusAndConstants = StringReplace[ToString[Append[Prepend[params/.relabel, OptionValue[Modulus]], I]], {"{" -> "(", "}" -> ")"}];
	ringVars = StringReplace[ToString[vars/.relabel], {"{" -> "(", "}" -> ")"}];
	(*  Create input string for Singular  *)
	stream = "ring r = " <> modulusAndConstants <> "," <> ringVars <> "," <> prepMonomialOrder[OptionValue[MonomialOrder]] <> ";"
		<> "poly p = " <> polyInStringForm <> ";"
		<> "ideal i = " <> StringReplace[idealInStringForm, {"{" -> "", "}" -> ""}] <> ";"
		<> "reduce(p, i);kill r;return()\n";
	tmpRes = singularInterpret[stream];
	ToExpression[tmpRes]/.(Reverse/@relabel)
];


setSingularPath = LibraryFunctionLoad["libSingularLink", "LL_setSingularPath", {"UTF8String"}, "Void"] 


LLSingularInterpret::usage = "Internal c++ wrapper"
LLSingularInterpret::author = "Gregor Kaelin"
LLSingularInterpret = LibraryFunctionLoad["libSingularLink", "LL_singularInterpret", {"UTF8String"}, "UTF8String"];

singularInterpret::usage = "Interpret a full procedure by the singular interpreter"
singularInterpret::author = "Gregor Kaelin"
singularInterpret[args_String] := LLSingularInterpret[args]


relabelVars::usage = "Default relabeling of variables for singular"
relabelVars::author = "Alessandro Georgoudis"
relabelVars[Vars_List, LabelName_String] := Module[{c},
	c = 1;
	Return[# -> ToExpression[LabelName <> ToString[c++]] & /@ Vars];
];


makeString::usage = "Produces the string obtained by concatenation of the arguments. Arguments which are not strings are converted into strings."
makeString::author = "Gregor Kaelin, Singular.m" 
makeString[args___] := 
    Apply[StringJoin,
        Map[ If[Head[#]=!=String, ToString[#, FormatType -> InputForm], #]&,{args}]
    ]


prepMonomialOrder::usage = "Prepares the monomial order Mathematic->Singular"
prepMonomialOrder::author = "Gregor Kaelin, Singular.m"
prepMonomialOrder[order_] := Module[{tmp}, 
	tmp = order/.{
		Automatic -> DegreeReverseLexicographic,
		DegreeReverseLexicographic -> "dp",
		DegreeReverseLexicographicSeries -> "ds", 
		DegreeLexicographic -> "Dp", 
		DegreeLexicographicSeries -> "Ds", 
		Lexicographic -> "lp", 
		LexicographicSeries -> "ls"};
	tmp = Which[
		MemberQ[{"dp", "ds", "Dp", "Ds", "lp", "ls"}, tmp], 
		(* one block with standard order only *)		
		tmp,
		
		Head[tmp] === List,
		(* block order *)
		"M" <> makeString[Flatten[tmp]]
     ];
     StringReplace[tmp, {"{" -> "(", "}" -> ")", "\"" -> "", "[" -> "(", "]" -> ")"}]
]

End[]


Protect@@Names["singularLink`*"];
EndPackage[]

