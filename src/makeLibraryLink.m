(* ::Package:: *)

Quit[]


(* ::Section:: *)
(*Produce cpp code*)


<<Amplitudes`cppLibraryLink`


classes = {};
functions = {
	cppFunction["singularInterpret", {"UTF8String"}, "UTF8String"]
};
includes = {"Singular/libsingular.h"};


Export[NotebookDirectory[]<>"libraryLink.hpp",generateHpp[classes,functions,includes],"String"];
Export[NotebookDirectory[]<>"libraryLink.cpp",generateCpp[classes,functions,includes],"String"];
