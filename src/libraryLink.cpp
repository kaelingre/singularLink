#if HAVE_CONFIG_H
#  include <config.h>
#endif

#include "libraryLink.hpp"

#include <Singular/libsingular.h>
#include <cstring>
#include <sstream>
#include <vector>
#include <iostream>

//********** singularInterpret **********
EXTERN_C DLLEXPORT int LL_singularInterpret(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_) {
	//load singular library
	if (!LL_libIsLoaded) {
		LL_libIsLoaded = true;
		//init singular
		char* str = new char[LL_libSingularPath.length() + 1];
		std::strcpy(str, LL_libSingularPath.c_str());
		siInit(str);

		delete [] str;

		//initialize return callback
		PrintS_callback = addChars;
		WerrorS_callback = NULL;

		//load general.lib and set options
		iiAllStart(NULL, (char*) "option(redSB);option(redTail);option(notWarnSB);", BT_proc, 0);
	}

	//clear return char
	if (LL_retChar) {
		delete [] LL_retChar;
	}

	//reset singular callback string
	LL_str.clear();

	//get argument
	char* arg0 = MArgument_getUTF8String(args_[0]);
	
	//call singular
	iiAllStart(NULL, arg0, BT_proc, 0);

	//set return char
	LL_retChar = new char[LL_str.length() + 1];
	std::strcpy(LL_retChar, LL_str.c_str());
	MArgument_setUTF8String(res_, LL_retChar);

	//clean up argument
	libData_->UTF8String_disown(arg0);

	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT int LL_setSingularPath(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_) {
	char* arg0 = MArgument_getUTF8String(args_[0]);
	LL_libSingularPath = arg0;

	libData_->UTF8String_disown(arg0);

	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_) {
	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_) {
}
