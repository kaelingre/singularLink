#ifndef LIBRARYLINK_HPP
#define LIBRARYLINK_HPP

#include <map>
#include <string>
#include "WolframLibrary.h"

static bool LL_libIsLoaded = false;
static std::string LL_str;
static std::string LL_libSingularPath("/usr/local/lib/libSingular.so");
static char* LL_retChar;

void addChars(const char* s_) {
	LL_str += s_;
}

EXTERN_C DLLEXPORT int WolframLibrary_getVersion() {
	return WolframLibraryVersion;
}

//********** singularInterpret **********
EXTERN_C DLLEXPORT int LL_singularInterpret(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_);

EXTERN_C DLLEXPORT int LL_setSingularPath(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_);

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_);

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_);

#endif /*LIBRARYLINK_HPP*/
