
#include <Singular/libsingular.h>
#include <iostream>

int main () {
	siInit((char *) "/usr/lib/libSingular.so");

	//create a ring
	char** n = (char**)omAlloc(2*sizeof(char*));
	n[0]=(char*)"x";
	n[1]=(char*)"y";
	ring r = rDefault(0,2,n);
	//more general:
	//ring r = rInit();
	//n is not needed anymore
	omFreeSize(n, 2*sizeof(char*));
	//make it default ring
	rChangeCurrRing(r);

	return 0;
}
